module.exports = function(baseDir, packageJSON) {
  const data= require('gulp-data');
  const del = require('del');
  const gulp = require('gulp');
  const gulpif = require('gulp-if');
  const flatten = require('gulp-flatten');
  const fs = require('fs');
  const plumber = require('gulp-plumber');
  const chokidar = require('chokidar');
  const postcss = require('gulp-postcss');
  const cssnano = require('cssnano');
  const colorFunction = require('postcss-color-function');
  const mediaMove = require('postcss-move-media');
  const path = require('path');
  const postcssImport = require('postcss-import');
  const postcssPresetEnv = require('postcss-preset-env');
  const rename = require('gulp-rename');
  const replace = require('gulp-replace');
  const rev = require('gulp-rev');
  const revReplace = require('gulp-rev-replace');
  const simpleVars = require('postcss-simple-vars');
  const sourcemaps = require('gulp-sourcemaps');
  const template = require('gulp-template');
  const wrap = require('gulp-wrap');


  const argv = require('yargs').argv;

  const brand = packageJSON.name;

  // Пути к файлам
  const paths = {
    build: {
      root: path.join(baseDir, '../project'),
      data: path.join(baseDir,'../project/data/production'),
      base: path.join(baseDir,'../project/assets'),
      css: path.join(baseDir,'../project/assets/css'),
      json: path.join(baseDir, '../project/assets/json'),
      fonts: path.join(baseDir, '../project/assets/fonts'),
      img: path.join(baseDir, '../project/assets/img'),
      js: path.join(baseDir, '../project/assets/js'),
      pages: path.join(baseDir, '../project/templates/pages'),
      parts: path.join(baseDir, '../project/templates/parts'),
    },
    src: {
      data: path.join(baseDir, '../project/data/content'),
      brands: path.join(baseDir, './src/assets/brands'),
      css: path.join(baseDir, './src/assets/css'),
      components: path.join(baseDir, './src/components'),
      fonts: path.join(baseDir, './src/assets/fonts'),
      js: path.join(baseDir, './src/assets/js'),
      img: path.join(baseDir, './src/assets/img'),
      initFiles: path.join(baseDir,'./src/assets/init-files'),
      pages: path.join(baseDir, './src/pages'),
    },
  };

  /* =========================================================
   Helpers
   =========================================================*/
  function fixRevManifest(file) {
    const obj = JSON.parse(fs.readFileSync(file, 'utf8'));
    Object.keys(obj).forEach((item) => {
      if (item.indexOf('.css') !== -1) {
        obj[item] = item;
      }
    });

    fs.writeFileSync(file, JSON.stringify(obj));
  }

  /* =========================================================
   DATA
   =========================================================*/
  gulp.task('copy-data', () => (
    gulp.src([
      `${paths.src.data}/*.json`,
    ])
      .pipe(plumber())
      .pipe(rename((pathName) => {
        pathName.basename += '-production'; // eslint-disable-line
      }))
      .pipe(gulp.dest(paths.build.data))
  ));

  gulp.task('watch-data', () => {
    gulp.watch([
      `${paths.src.data}/*.json`,
    ], gulp.series('copy-data'));
  });

  /* =========================================================
   Components
   =========================================================*/

   function createPartsMarkup(path, name) {
     fs.writeFileSync(`${path}/${name}.twig`,
       `<section class="${name}">\n</section>`
     );
   }

   function createPartsStyle(path, name) {
     fs.writeFileSync(`${path}/${name}.css`,
       `.${name} {\n}`
     );
   }

   function createPartsScript(path, name) {
     fs.writeFileSync(`${path}/${name}.js`,
       `export default () => {\n}`
     );
   }

   gulp.task('watch-parts', () => {
     const watcherParts = chokidar.watch(`${paths.src.components}`, {ignored: /^\./, persistent: true});
     watcherParts.on('ready', () => {

      watcherParts.on('addDir', folderPath => {
        const dirName = folderPath.split('/').reverse()[0];
        createPartsMarkup(folderPath, dirName);
        createPartsStyle(folderPath, dirName);
        createPartsScript(folderPath, dirName);
      })
    })
  });

  /* =========================================================
   Markup
   =========================================================*/

  gulp.task('copy-parts', () => (
    gulp.src([
      `${paths.src.components}/**/*.twig`,
    ])
      .pipe(plumber())
      .pipe(flatten({ includeParents: 0 }))
      .pipe(gulp.dest(paths.build.parts))
  ));

  gulp.task('copy-pages', () => (
    gulp.src([
      `${paths.src.pages}/**/*.twig`,
    ], { base: `${paths.src}/pages` })
      .pipe(plumber())
      .pipe(flatten({ includeParents: 0 }))
      .pipe(gulp.dest(paths.build.pages))
  ));

  gulp.task('copy-markup', gulp.parallel(
    'copy-parts',
    'copy-pages',
  ));

  gulp.task('watch-markup', () => {
    gulp.watch([
      `${paths.src.components}/**/*.twig`,
      `${paths.src.pages}/**/*.twig`,
    ], gulp.series('copy-markup'));
  });


  /* =========================================================
   Стили
   =========================================================*/

  // Postcss плагины
  const processors = [
    simpleVars({
      variables: {
        brand: brand,
      },
    }),
    postcssImport(),
    postcssPresetEnv({
      stage: 0,
      preserve: false
    }),
    colorFunction(),
  ];

  if (argv.production) {
    processors.push(mediaMove());
    processors.push(cssnano({
      zindex: false,
      reduceIdents: false,
      discardUnused: {
        fontFace: false,
      },
    }));
  }

  // Сборка основных стилей
  function addCSSImport(file, type) {
    const fileName = file.split('/')[0];
    const contentJSON = JSON.parse(fs.readFileSync(`${paths.build.root}/data/content/${fileName}.json`));
    return contentJSON[type].reduce((acc, item) => {
      return `${acc}
        @import "../../components/${item.name}/${item.name}.css";
      `
    }, '');
  }

  gulp.task('main-css', () => {
    if (argv.production) {
      del([
        `${paths.build.css}/*`,
      ], { force: true });
    }

    fixRevManifest(`${paths.build.json}/rev-manifest.json`);

    const manifest = gulp.src(`${paths.build.json}/rev-manifest.json`);

    return gulp.src([
      `${paths.src.pages}/**/*.css`,
      `!${paths.src.pages}/**/*-critical.css`,
    ])
      .pipe(plumber())
      .pipe(sourcemaps.init())
      .pipe(replace('<%import%>', function() {
        return addCSSImport(this.file.relative, 'secondaryScreen')
      }))
      .pipe(postcss(processors))
      .pipe(gulpif(argv.production, postcss(processors)))
      .pipe(gulpif(!argv.production, sourcemaps.write('./')))
      .pipe(revReplace({ manifest }))
      .pipe(gulpif(argv.production, rev()))
      .pipe(flatten({ includeParents: 0 }))
      .pipe(gulp.dest(paths.build.css))
      .pipe(gulpif(argv.production, rev.manifest(`${paths.build.json}/rev-manifest.json`, {
        merge: true,
        base: paths.build.json,
      })))
      .pipe(gulpif(argv.production, gulp.dest(paths.build.json)));
  });



  // Сборка стилей для первого экрана
  gulp.task('critical-css', (e) => {
    const manifest = gulp.src(`${paths.build.json}/rev-manifest.json`);
    return gulp.src(`${paths.src.pages}/**/*-critical.css`)
      .pipe(plumber())
      .pipe(replace('<%import%>', function() {
        return addCSSImport(this.file.relative, 'firstScreen')
      }))
      .pipe(postcss(processors))
      .pipe(gulpif(argv.production, postcss(processors)))
      .pipe(revReplace({ manifest }))
      .pipe(replace('../', './'))
      .pipe(wrap('<style><%= contents %></style>'))
      .pipe(rename((pathName) => {
        pathName.extname = '.twig'; // eslint-disable-line
      }))
      .pipe(flatten({ includeParents: 0 }))
      .pipe(gulp.dest(paths.build.parts));
  });

  // Watcher для стилей
  gulp.task('watch-css', () => {
    gulp.watch([
      `${paths.src.css}/**/*.css`,
      `${paths.src.pages}/**/*.css`,
      `${paths.src.components}/**/*.css`,
      `${paths.src.brands}/**/*.css`,
      `${paths.build.root}/data/content/*.json`,
    ], gulp.parallel('main-css', 'critical-css'));
  });

  // // Сборка стилей
  gulp.task('css', gulp.parallel('main-css', 'critical-css'));

  /* =========================================================
   Шрифты
   =========================================================*/
  gulp.task('fonts', () => (
    gulp.src(`${paths.src.fonts}/**/*`)
      .pipe(plumber())
      .pipe(rev())
      .pipe(gulp.dest(paths.build.fonts))
      .pipe(rev.manifest(`${paths.build.json}/rev-manifest.json`, {
        merge: true,
        base: paths.build.json,
      }))
      .pipe(gulp.dest(paths.build.json))
  ));


  /* =========================================================
   Копирование вендортных скриптов
   =========================================================*/

  gulp.task('copy-vendor-js', () => (
    gulp.src([
      path.join(baseDir, 'node_modules/jquery/dist/jquery.min.js'),
      `${paths.src.js}/vendor/*.js`
    ])
      .pipe(plumber())
      .pipe(gulp.dest(`${paths.build.js}/vendor`))
  ));


  /* =========================================================
   Копирование шаблонов скриптов страниц
   =========================================================*/

   gulp.task('copy-template-js', function() {
     return gulp.src([
       `${paths.src.pages}/**/*-template.js`,
     ])
       .pipe(data((file) => {
         const filePath = file.path.split('/');
         const fileName = filePath[filePath.length - 2];
         const contentJSON = JSON.parse(fs.readFileSync(`${paths.build.root}/data/content/${fileName}.json`));
         const modules = [...contentJSON['firstScreen'], ...contentJSON['secondaryScreen']].reduce((acc, item) => {
           const name = item.name;
           const scriptIsExixst = fs.existsSync(`${paths.src.components}/${name}/${name}.js`);
           if (scriptIsExixst && acc.indexOf(name)==-1) {
             acc.push(name);
           }
           return acc;
         }, []);

         return {modules};
       }))
       .pipe(template())
       .pipe(rename((pathName) => {
          pathName.basename = pathName.basename.replace('-template', ''); // eslint-disable-line
        }))
       .pipe(gulp.dest(`${paths.src.pages}`))
  });

   gulp.task('watch-template-js', () => {
    gulp.watch([
      `${paths.build.root}/data/content/*.json`,
    ], gulp.parallel('copy-template-js'));
  });


  /* =========================================================
  Копирование файлов конфигов которые не хранятся в репозитории
   =========================================================*/
  gulp.task('copy-rev-manifest', () => (
    gulp.src([
      `${paths.src.initFiles}/rev-manifest.json`,
    ])
      .pipe(plumber())
      .pipe(gulp.dest(paths.build.json))
  ));

  gulp.task('copy-init-files', gulp.parallel(
    'copy-rev-manifest',
  ));

  /* =========================================================
                  Очистка от старых файлов
   =========================================================*/
  gulp.task('clear-assets', () => (
    del([
      `${paths.build.base}/*`,
      `!${paths.build.base}/pic`,
    ], { force: true })
  ));

  gulp.task('clear-markup', () => (
    del([
      `${paths.build.parts}/*`,
      `${paths.build.pages}/*`,
    ], { force: true })
  ));

  gulp.task('clear', gulp.parallel(
    'clear-assets',
    'clear-markup',
  ));

  /* =========================================================
   Изображения
   =========================================================*/


  // Brand изображения
  gulp.task('brand-img', () => (
    gulp.src([
      `${paths.src.brands}/${brand}/img/*`,
      `!${paths.src.brands}/${brand}/img/favicons`,
    ])
      .pipe(plumber())
      .pipe(gulp.dest(`${paths.build.img}/brand`))
  ));

  /* =========================================================
   Favicons
   =========================================================*/
  gulp.task('favicons', () => (
    gulp.src([
      `${paths.src.brands}/${brand}/img/favicons/*`,
      `!${paths.src.brands}/${brand}/img/favicons/favicon.ico`,
      ])
      .pipe(plumber())
      .pipe(gulp.dest(`${paths.build.img}/favicons`))
  ));

  gulp.task('favicon-ico', () => (
    gulp.src(`${paths.src.brands}/${brand}/img/favicons/favicon.ico`)
      .pipe(plumber())
      .pipe(gulp.dest(`${paths.build.root}`))
  ));

  /* =========================================================
  Разработка проекта
   =========================================================*/
  gulp.task('watch', gulp.parallel(
    'watch-data',
    'watch-markup',
    'watch-css',
    'watch-template-js',
    'watch-parts',
  ));


  /* =========================================================
  Сборка проекта
   =========================================================*/
  gulp.task('build', gulp.series(
    'clear',
    'copy-init-files',
    'fonts',
    'brand-img',
    'favicons',
    'favicon-ico',
    'copy-markup',
    'css',
    'copy-vendor-js',
    'copy-template-js',
    'copy-data'
  ));
}
